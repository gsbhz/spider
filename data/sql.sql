-- 要抓取的网站信息配置
CREATE TABLE b_website (
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `name` VARCHAR(100) DEFAULT '' COMMENT '网站名',
    `url` VARCHAR(500) DEFAULT '' COMMENT '网站地址',
    `status` TINYINT DEFAULT 0 COMMENT '状态。0=不抓取；1=抓取',
    PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;


-- 分类
CREATE TABLE b_category (
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `wsid` INT COMMENT '对应的网站ID',
    `pid` INT DEFAULT 0 COMMENT '上级分类ID',
    `cname` VARCHAR(100) DEFAULT '' COMMENT '中文名',
    `ename` VARCHAR(100) DEFAULT '' COMMENT '英文名',
    `url` VARCHAR(500) DEFAULT '' COMMENT '地址',
    `origid` VARCHAR(50) DEFAULT '' COMMENT '网站上原先的ID',
    `status` TINYINT DEFAULT 0 COMMENT '状态。0=不抓取；1=抓取',
    PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

-- 品牌
CREATE TABLE b_brand (
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `wsid` INT COMMENT '对应的网站ID',
    `cname` VARCHAR(100) DEFAULT '' COMMENT '中文名',
    `ename` VARCHAR(100) DEFAULT '' COMMENT '英文名',
    `logo` VARCHAR(200) DEFAULT '' COMMENT '品牌Logo',
    `pinyin` VARCHAR(20) DEFAULT '' COMMENT '首字母',
    PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

-- 品牌-分类 关系表
CREATE TABLE b_category_brand_mapping (
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `wsid` INT COMMENT '对应的网站ID',
    `cid` INT COMMENT '分类ID',
    `brand_id` INT COMMENT '品牌ID',
    `url` VARCHAR(500) DEFAULT '' COMMENT '地址',
    `origid` VARCHAR(50) DEFAULT '' COMMENT '网站上原先的ID',
    `status` TINYINT DEFAULT 0 COMMENT '状态。0=不抓取；1=抓取',
    PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;


-- 产品
CREATE TABLE b_product (
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `wsid` INT COMMENT '对应的网站ID',
    `cid` INT COMMENT '分类ID',
    `brand_id` INT COMMENT '品牌ID',
    `name` VARCHAR(100) DEFAULT '' COMMENT '产品名',
    `price` VARCHAR(30) DEFAULT 0 COMMENT '价格',
    `desc` VARCHAR(2000) DEFAULT '' COMMENT '简介',
    -- `pics` VARCHAR(200) DEFAULT '' COMMENT '图片路径',
    `url` VARCHAR(500) DEFAULT '' COMMENT '地址',
    `origid` VARCHAR(50) DEFAULT '' COMMENT '网站上原先的ID',
    `status` INT DEFAULT 0 COMMENT '状态。0=未抓取；1=已抓取',
    PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

-- 产品图片
CREATE TABLE b_product_pic (
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `wsid` INT COMMENT '对应的网站ID',
    `prod_id` INT COMMENT '产品ID',
    `src` VARCHAR(100) COMMENT '图片路径',
    PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

-- 产品-参数关系表
CREATE TABLE b_product_param_mapping (
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `wsid` INT COMMENT '对应的网站ID',
    `prod_id` INT COMMENT '产品ID',
    `param_id` INT COMMENT '参数ID',
    `param_info_id` INT COMMENT '参数值',
    PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;


-- 参数类别
CREATE TABLE b_param_category (
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `wsid` INT COMMENT '对应的网站ID',
    `name` VARCHAR(100) COMMENT '参数类别名',
    PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

-- 参数表
CREATE TABLE b_param (
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `wsid` INT COMMENT '对应的网站ID',
    `prod_cid` INT COMMENT '产品分类ID',
    `param_cid` INT DEFAULT 0 COMMENT '参数类别ID',
    `name` VARCHAR(100) COMMENT '参数名',
    PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

-- 参数值表
CREATE TABLE b_param_info (
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `wsid` INT COMMENT '对应的网站ID',
    `param_id` INT COMMENT '参数表ID',
    `text` VARCHAR(100) COMMENT '参数内容',
    PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;









/*==================================================================*/
SELECT p.id, p.`name`, c.`cname`, c.`ename`, b.`cname`, b.`ename`
FROM b_product p
    INNER JOIN b_category c ON c.`id` = p.`cid`
    INNER JOIN b_brand b ON b.`id` = p.`brand_id`
WHERE p.origid = 731510;


SELECT p.`name`, i.`text`
FROM b_param p
    INNER JOIN b_param_info i ON i.`param_id` = p.`id`
    INNER JOIN `b_product_param_mapping` m ON m.`param_id` = p.`id` AND m.`param_info_id` = i.`id`
    INNER JOIN b_product pr ON pr.`id` = m.`prod_id`
WHERE pr.origid = 731510;


-- 京东 -- 
-- 查询分类名，参数名，参数值
SELECT c.`cname`, p.`name`, GROUP_CONCAT(i.`text` ORDER BY i.id ASC) AS param_value
FROM b_category c 
    INNER JOIN b_param p ON p.`prod_cid` = c.`id`
    INNER JOIN b_param_info i ON i.`param_id` = p.`id`
WHERE c.id = 3
GROUP BY c.`cname`, p.`name`
ORDER BY p.`id` ASC;




-- ============================================= --
SELECT * FROM b_category WHERE wsid = 2;
SELECT * FROM b_brand WHERE wsid = 2;
SELECT * FROM b_category_brand_mapping WHERE wsid = 2;

SELECT * FROM b_product ORDER BY id DESC;
SELECT * FROM b_product_pic ORDER BY id DESC;
SELECT * FROM b_param_category ORDER BY id DESC;
SELECT * FROM b_param ORDER BY id DESC;
SELECT * FROM b_param_info ORDER BY id DESC;
SELECT * FROM b_product_param_mapping ORDER BY id DESC;



-- 接口调用日志
CREATE TABLE api_log(
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `controller` VARCHAR(100) DEFAULT '',
    `action` VARCHAR(100) DEFAULT '',
    `count` INT DEFAULT 0 COMMENT '计数',
    PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;


/*
-- DELETE FROM b_category WHERE wsid = 2;
DELETE FROM b_brand WHERE wsid = 2;
DELETE FROM b_category_brand_mapping WHERE wsid = 2;


TRUNCATE table b_category;
TRUNCATE TABLE b_brand;
TRUNCATE TABLE b_category_brand_mapping;

TRUNCATE TABLE b_product;
TRUNCATE TABLE b_product_pic;
TRUNCATE TABLE b_param_category ;
TRUNCATE TABLE b_param ;
TRUNCATE TABLE b_param_info;
TRUNCATE TABLE b_product_param_mapping;
*/


-- 原先的品牌错误，修复脚本
-- UPDATE b_product SET brand_id = (SELECT cb.brand_id FROM b_category_brand_mapping cb WHERE cb.id = b_product.brand_id);






